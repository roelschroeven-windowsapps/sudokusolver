//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SudokuView.h"
#include "Util.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TSudokuView::TSudokuView(TComponent *Owner):
  TCustomControl(Owner),
  m_Selection(-1, -1),
  Playing(false),
  m_UpdateLevel(0)
{
  Cells.OnChangeCell = OnChangeCell;
  Cells.OnChangeAll = OnChangeAll;
}

__fastcall TSudokuView::~TSudokuView()
{
}

static void DrawLine(TCanvas *Canvas, int x1, int y1, int x2, int y2)
{
  Canvas->MoveTo(x1, y1);
  Canvas->LineTo(x2, y2);
}

void TSudokuView::DrawGrid()
{
  Canvas->Pen->Style = psSolid;
  Canvas->Pen->Color = clGray;

  int CellWidth = m_GridRect.Width() / SUDOKU_COLS;
  for (int i = 0; i <= SUDOKU_COLS; ++i)
  {
    if (i % 3 == 0)
      Canvas->Pen->Width = 3;
    else
      Canvas->Pen->Width = 1;
    int x = m_GridRect.left + i * CellWidth;
    DrawLine(Canvas, x, m_GridRect.top, x, m_GridRect.bottom);
  }

  int CellHeight = m_GridRect.Height() / SUDOKU_ROWS;
  for (int i = 0; i <= SUDOKU_ROWS; ++i)
  {
    if (i % 3 == 0)
      Canvas->Pen->Width = 3;
    else
      Canvas->Pen->Width = 1;
    int y = m_GridRect.top + i * CellHeight;
    DrawLine(Canvas, m_GridRect.left, y, m_GridRect.right, y);
  }
}

static TRect GetCellRect(const TRect &GridRect, int row, int col)
{
  const int Padding = 4;
  int CellWidth = GridRect.Width() / SUDOKU_COLS;
  int CellHeight = GridRect.Height() / SUDOKU_ROWS;
  TRect CellRect;
  CellRect.left = GridRect.left + col * CellWidth + Padding;
  CellRect.right = GridRect.left + (col + 1) * CellWidth - Padding;
  CellRect.top = GridRect.top + row * CellHeight + Padding;
  CellRect.bottom = GridRect.top + (row + 1) * CellHeight - Padding;
  return CellRect;
}

void TSudokuView::DrawCell(int row, int col)
{
  TRect CellRect = GetCellRect(m_GridRect, row, col);

  // First clear the cell
  Canvas->Brush->Style = bsSolid;
  Canvas->FillRect(CellRect);

  // Draw the new contents: text, focus indicator

  const TSudokuCell &Cell(Cells.Cell[row][col]);
  if (Cell.Known)
  {
    // Text
    Canvas->Font->Name = "Arial";
    Canvas->Font->Color = Cell.Given ? clBlack : clBlue;
    Canvas->Font->Height = -(CellRect.Height() * 0.8 - 2);
    Canvas->Brush->Style = bsClear;
    TSize Size = Canvas->TextExtent(Cell.ValueString());
    int x = CellRect.left + (CellRect.Width() - Size.cx) / 2;
    int y = CellRect.top + (CellRect.Height() - Size.cy) / 2;
    Canvas->TextOut(x, y, Cell.ValueString());
  }

  if (row == m_Selection.row && col == m_Selection.col)
  {
    // Focus indicator
    Canvas->Pen->Width = 1;
    Canvas->Pen->Style = psDot;
    Canvas->Pen->Color = clGray;
    Canvas->Brush->Style = bsClear;
    Canvas->Rectangle(CellRect);
  }
}

void TSudokuView::DrawCells()
{
  for (int row = 0; row < SUDOKU_HEIGHT; ++row)
    for (int col = 0; col < SUDOKU_WIDTH; ++col)
      DrawCell(row, col);
}

void __fastcall TSudokuView::Paint()
{
  // Provide some margins, adjust the size to a multiple of 9, center the raster
  const int Margin = 10;
  m_GridRect = ClientRect;
  m_GridRect.right -= Margin * 2;
  m_GridRect.right -= m_GridRect.Width() % SUDOKU_WIDTH;
  int HorzOffset = (ClientWidth - m_GridRect.Width()) / 2;
  m_GridRect.left += HorzOffset;
  m_GridRect.right += HorzOffset;
  m_GridRect.bottom -= Margin * 2;
  m_GridRect.bottom -= m_GridRect.Height() % SUDOKU_HEIGHT;
  int VertOffset = (ClientHeight - m_GridRect.Height()) / 2;
  m_GridRect.top += VertOffset;
  m_GridRect.bottom += VertOffset;

  DrawGrid();
  DrawCells();
}
//---------------------------------------------------------------------------

void TSudokuView::SetSelected(TSudokuView::TPos Selection)
{
  if (Selection != m_Selection)
  {
    TPos OldSelection = Selection;
    m_Selection = Selection;
    DrawCell(OldSelection.row, OldSelection.col);
    DrawCell(Selection.row, Selection.col);
  }
}
//---------------------------------------------------------------------------

void __fastcall TSudokuView::WndProc(Messages::TMessage &Message)
{
  // Tell Windows that we want to receive keyboard events for the arrow keys, tab, ...
  if (Message.Msg == WM_GETDLGCODE)
  {
    Message.Result = DLGC_WANTARROWS | DLGC_WANTTAB | DLGC_WANTCHARS;
    return;
  }

  TCustomControl::WndProc(Message);
}

void __fastcall TSudokuView::KeyDown(Word &Key, Classes::TShiftState Shift)
{
  TCustomControl::KeyDown(Key, Shift);

  switch (Key)
  {
  case VK_LEFT:  MoveSelection(-1, 0); break;
  case VK_RIGHT: MoveSelection(+1, 0); break;
  case VK_UP:    MoveSelection(0, -1); break;
  case VK_DOWN:  MoveSelection(0, +1); break;
  case VK_TAB:
    if (Shift == TShiftState())
    {
      if (m_Selection.col == SUDOKU_COLS - 1)
        MoveSelection(+1, +1);
      else
        MoveSelection(+1, 0);
    }
    else if (Shift == TShiftState() << ssShift)
    {
      if (m_Selection.col == 0)
        MoveSelection(-1, -1);
      else
        MoveSelection(-1, 0);
    }
    break;

  case VK_DELETE:
    {
      if (!Playing || !Cells.Given[m_Selection.row][m_Selection.col])
        Cells.Known[m_Selection.row][m_Selection.col] = false;
      else
        Beep();
    }
    break;
  }
}

void __fastcall TSudokuView::KeyPress(System::WideChar &Key)
{
  TCustomControl::KeyPress(Key);
  if (Key >= '0' && Key <= '9')
  {
    if (!Playing || !Cells.Given[m_Selection.row][m_Selection.col])
    {
      Cells.Value[m_Selection.row][m_Selection.col] = Key;
      Cells.Given[m_Selection.row][m_Selection.col] = !Playing;
      Cells.Known[m_Selection.row][m_Selection.col] = true;
    }
    else
      Beep();
  }
  else if (Key == ' ')
  {
    if (!Playing || !Cells.Given[m_Selection.row][m_Selection.col])
      Cells.Known[m_Selection.row][m_Selection.col] = false;
    else
      Beep();
  }
}

void __fastcall TSudokuView::MouseUp(TMouseButton Button, Classes::TShiftState Shift, int X, int Y)
{
  // TODO: calculate row and column directly instead of trying all the cell rects
  for (int row = 0; row < SUDOKU_ROWS; ++row)
  {
    for (int col = 0; col < SUDOKU_COLS; ++col)
    {
    TRect CellRect = GetCellRect(m_GridRect, row, col);
    if (X >= CellRect.left && X < CellRect.right && Y >= CellRect.top && Y < CellRect.bottom)
      {
      SetSelection(row, col);
      break;
      }
    }
  }
}

void TSudokuView::BeginUpdate()
{
  ++m_UpdateLevel;
}

void TSudokuView::EndUpdate()
{
  --m_UpdateLevel;
  if (m_UpdateLevel <= 0)
    Repaint();
}

void TSudokuView::OnChangeCell(int row, int col)
{
  if (m_UpdateLevel <= 0)
    DrawCell(row, col);
}

void TSudokuView::OnChangeAll()
{
  if (m_UpdateLevel <= 0)
    Repaint();
}

void TSudokuView::MoveSelection(int HorzDelta, int VertDelta)
{
  SetSelection(
    (m_Selection.row + SUDOKU_ROWS + VertDelta) % SUDOKU_ROWS,
    (m_Selection.col + SUDOKU_COLS + HorzDelta) % SUDOKU_COLS
    );
}

void TSudokuView::SetSelection(int row, int col)
{
  TPos OldSelection = m_Selection;
  m_Selection.col = col;
  m_Selection.row = row;
  DrawCell(OldSelection.row, OldSelection.col);
  DrawCell(m_Selection.row, m_Selection.col);
}
