#include <vcl.h>
#pragma hdrstop

#include <stdio.h>

#include "SudokuModel.h"
#include "SolveBruteforce.h"

void print_usage()
{
  printf("Usage:\n");
  printf("\n");
  printf("SudokuSolverCmd <sudoku file>\n");
  printf("\n");
}

void print_sudoku(const TSudokuModel &Model)
{
  for (int row = 0; row < 9; ++row)
  {
    for (int col = 0; col < 9; ++col)
    {
      if (Model.Given[row][col])
        printf("[%s]", Model.ValueString[row][col].c_str());
      else
        printf(" %s ", Model.ValueString[row][col].c_str());
      if (col == 2 || col == 5)
        printf("|");
    }
    printf("\n");
    if (row == 2 || row == 5)
      printf("---------+---------+---------\n");
  }
}

int _tmain(int argc, _TCHAR* argv[])
{
  if (argc != 2)
    {
    print_usage();
    return 1;
    }

  if (!FileExists(argv[1]))
    {
    fprintf(stderr, "Can't find file: %s\n", argv[1]);
    return 1;
    }

  TSudokuModel Model;
  try
  {
    Model.LoadFromFile(argv[1]);
  }
  catch (Exception &E)
  {
    fprintf(stderr, "%s\n", E.Message.c_str());
    return 1;
  }

  bool Success = SolveBruteforce(Model);
  if (Success)
    print_sudoku(Model);
  else
    printf("Failed to solve\n");
  return 0;
}
//---------------------------------------------------------------------------
 