object FormMain: TFormMain
  Left = 245
  Top = 134
  Caption = 'Sudoku Solver'
  ClientHeight = 512
  ClientWidth = 472
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBottomBar: TPanel
    Left = 0
    Top = 452
    Width = 472
    Height = 41
    Align = alBottom
    TabOrder = 1
    object ButtonSolve: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Solve'
      Default = True
      TabOrder = 0
      TabStop = False
      OnClick = ButtonSolveClick
    end
    object ButtonCancel: TButton
      Left = 88
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      TabOrder = 1
      TabStop = False
      Visible = False
      OnClick = ButtonCancelClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 493
    Width = 472
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object MainMenu: TMainMenu
    Left = 16
    Top = 8
    object File1: TMenuItem
      Caption = 'File'
      object Open1: TMenuItem
        Caption = 'Open...'
        OnClick = Open1Click
      end
      object Opensamplepuzzle1: TMenuItem
        Caption = 'Open sample puzzle'
        OnClick = Opensamplepuzzle1Click
      end
      object Save1: TMenuItem
        Caption = 'Save'
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = 'Save as...'
        OnClick = SaveAs1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
    object Sudoku1: TMenuItem
      Caption = 'Edit'
      object Clear1: TMenuItem
        Caption = 'Clear'
        OnClick = Clear1Click
      end
      object Reset1: TMenuItem
        Caption = 'Reset'
        OnClick = Reset1Click
      end
    end
    object Settings1: TMenuItem
      Caption = 'Settings'
      object Showprogress1: TMenuItem
        Caption = 'Show progress'
        OnClick = Showprogress1Click
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object About1: TMenuItem
        Caption = 'About'
        OnClick = About1Click
      end
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'sud'
    Filter = 'Sudoku puzzles (*.sud)|*.sud|All files|*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Open Sudoku Puzzle'
    Left = 56
    Top = 8
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'sud'
    Filter = 'Sudoku puzzles (*.sud)|*.sud|All files|*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Sudoku Puzzle'
    Left = 88
    Top = 8
  end
end
