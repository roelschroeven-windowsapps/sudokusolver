//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "About.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAbout *FormAbout;
//---------------------------------------------------------------------------

void ShowAbout()
{
  TFormAbout *FormAbout;
  Application->CreateForm(__classid(TFormAbout), &FormAbout);
  FormAbout->ShowModal();
  delete FormAbout;
}
//---------------------------------------------------------------------------

struct TVersionInfo { DWORD v1; DWORD v2; DWORD v3; DWORD v4; };

static TVersionInfo ExeVersion()
{
  TVersionInfo VersionInfo = { 0, 0, 0, 0 };
  AnsiString Filename = Application->ExeName;
  DWORD Handle;
  DWORD Size =  GetFileVersionInfoSize(Filename.c_str(), &Handle);
  if (Size > 0)
  {
    char *FileVersionInfoData = new char[Size + 1];
    if (GetFileVersionInfo(Filename.c_str(), Handle, Size, FileVersionInfoData))
    {
      VS_FIXEDFILEINFO FixedFileInfo;
      LPVOID Buffer;
      UINT BufferLength;
      Win32Check(VerQueryValue(FileVersionInfoData, "\\", &Buffer, &BufferLength));
      memmove(&FixedFileInfo, Buffer, BufferLength);
      VersionInfo.v1 = HIWORD(FixedFileInfo.dwFileVersionMS);
      VersionInfo.v2 = LOWORD(FixedFileInfo.dwFileVersionMS);
      VersionInfo.v3 = HIWORD(FixedFileInfo.dwFileVersionLS);
      VersionInfo.v4 = LOWORD(FixedFileInfo.dwFileVersionLS);
    }
    delete [] FileVersionInfoData;
  }
  return VersionInfo;
}

static AnsiString ExeVersionString()
{
  AnsiString result;
  TVersionInfo VersionInfo = ExeVersion();
  result.printf("%u.%u.%u.%u", VersionInfo.v1, VersionInfo.v2, VersionInfo.v3, VersionInfo.v4);
  return result;
}


__fastcall TFormAbout::TFormAbout(TComponent* Owner)
  : TForm(Owner)
{
  AnsiString FirstLine = MemoCopyright->Lines->Strings[0] + ' ' + ExeVersionString();
  MemoCopyright->Lines->Strings[0] = FirstLine;
}
//---------------------------------------------------------------------------
