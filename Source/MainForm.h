//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#ifndef MainFormH
#define MainFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <AppEvnts.hpp>
//---------------------------------------------------------------------------

class TSudokuView;
class TSudokuModel;

class TFormMain : public TForm
{
__published:	// IDE-managed Components
  TPanel *PanelBottomBar;
  TStatusBar *StatusBar;
  TButton *ButtonSolve;
  TButton *ButtonCancel;
  TMainMenu *MainMenu;
  TMenuItem *File1;
  TMenuItem *Open1;
  TMenuItem *Save1;
  TMenuItem *SaveAs1;
  TMenuItem *Exit1;
  TMenuItem *Sudoku1;
  TMenuItem *Clear1;
  TMenuItem *Reset1;
  TMenuItem *Settings1;
  TMenuItem *Showprogress1;
  TOpenDialog *OpenDialog;
  TSaveDialog *SaveDialog;
  TMenuItem *Opensamplepuzzle1;
  TMenuItem *N1;
  TMenuItem *Help1;
  TMenuItem *About1;
  void __fastcall ButtonSolveClick(TObject *Sender);
  void __fastcall Open1Click(TObject *Sender);
  void __fastcall Save1Click(TObject *Sender);
  void __fastcall SaveAs1Click(TObject *Sender);
  void __fastcall Exit1Click(TObject *Sender);
  void __fastcall Clear1Click(TObject *Sender);
  void __fastcall Reset1Click(TObject *Sender);
  void __fastcall Showprogress1Click(TObject *Sender);
  void __fastcall ButtonCancelClick(TObject *Sender);
  void __fastcall Opensamplepuzzle1Click(TObject *Sender);
  void __fastcall About1Click(TObject *Sender);
  void __fastcall FormKeyPress(TObject *Sender, System::WideChar &Key);
  void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);

private:	// User declarations
  TSudokuView *m_SudokuView;
  AnsiString m_Filename;
  bool m_RequestToCancel;

  void SaveAs();
  void Callback(const TSudokuModel &M, bool &Stop);

public:		// User declarations
  __fastcall TFormMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMain *FormMain;
//---------------------------------------------------------------------------
#endif
