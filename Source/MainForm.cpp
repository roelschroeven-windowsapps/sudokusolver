//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainForm.h"
#include "About.h"
#include "SudokuView.h"
#include "SolveBruteforce.h"
#include "Util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormMain *FormMain;
//---------------------------------------------------------------------------

static AnsiString GetApplicationPath()
{
  return ExtractFilePath(Application->ExeName);
}
//---------------------------------------------------------------------------

__fastcall TFormMain::TFormMain(TComponent* Owner):
  TForm(Owner),
  m_RequestToCancel(false)
{
  m_SudokuView = new TSudokuView(this);
  m_SudokuView->Parent = this;
  m_SudokuView->Align = alClient;
  m_SudokuView->DoubleBuffered = true;
  m_SudokuView->Selected = TSudokuView::TPos(0, 0);
  m_SudokuView->TabStop = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonSolveClick(TObject *Sender)
{
  m_SudokuView->Cells.ResetToGivens();
  TSudokuModel WorkingCells(m_SudokuView->Cells);

  m_RequestToCancel = false;
  ButtonSolve->Enabled = false;
  ButtonCancel->Visible = true;

  StatusBar->SimpleText = "Working ...";
  THighPrecTimer Timer(true);
  bool Solved = SolveBruteforce(WorkingCells, Callback);
  Timer.Stop();

  ButtonSolve->Enabled = true;
  ButtonCancel->Visible = false;

  m_SudokuView->Cells = WorkingCells;

  if (Solved)
    StatusBar->SimpleText = AnsiPrintF("Solved in %0.3lf seconds", Timer.Seconds());
  else
    StatusBar->SimpleText = AnsiPrintF("No solution found (%0.3lf seconds)", Timer.Seconds());
}
//---------------------------------------------------------------------------

void TFormMain::Callback(const TSudokuModel &M, bool &Stop)
{
  if (Showprogress1->Checked)
  {
    m_SudokuView->Cells = M;
    m_SudokuView->Repaint();
  }
  Application->ProcessMessages();
  if (m_RequestToCancel)
    Stop = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Open1Click(TObject *Sender)
{
  if (OpenDialog->Execute())
  {
    m_Filename = OpenDialog->FileName;
    m_SudokuView->Cells.LoadFromFile(m_Filename);
  }
}
//---------------------------------------------------------------------------

void TFormMain::SaveAs()
{
  if (SaveDialog->Execute())
  {
    m_Filename = SaveDialog->FileName;
    m_SudokuView->Cells.SaveToFile(m_Filename);
  }
}

//---------------------------------------------------------------------------

void __fastcall TFormMain::Save1Click(TObject *Sender)
{
  if (m_Filename == "")
    SaveAs();                               
  else
    m_SudokuView->Cells.SaveToFile(m_Filename);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::SaveAs1Click(TObject *Sender)
{
  SaveAs();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Exit1Click(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Clear1Click(TObject *Sender)
{
  m_SudokuView->Cells.Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Reset1Click(TObject *Sender)
{
  m_SudokuView->Cells.ResetToGivens();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Showprogress1Click(TObject *Sender)
{
  Showprogress1->Checked = ! Showprogress1->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonCancelClick(TObject *Sender)
{
  m_RequestToCancel = true;  
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Opensamplepuzzle1Click(TObject *Sender)
{
  m_SudokuView->Cells.LoadFromFile(GetApplicationPath() + "Sample puzzle.sud");
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::About1Click(TObject *Sender)
{
  ShowAbout();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormKeyPress(TObject *Sender, System::WideChar &Key)
{
  if (!m_SudokuView->Focused())
    m_SudokuView->KeyPress(Key);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (!m_SudokuView->Focused())
    m_SudokuView->KeyDown(Key, Shift);
}
//---------------------------------------------------------------------------

