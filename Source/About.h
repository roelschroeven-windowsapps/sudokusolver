//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#ifndef AboutH
#define AboutH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------

void ShowAbout();

class TFormAbout : public TForm
{
__published:	// IDE-managed Components
  TPanel *PanelCopyright;
  TImage *ImageIcon;
  TMemo *MemoCopyright;
  TMemo *MemoGpl;
  TButton *ButtonClose;
private:	// User declarations
public:		// User declarations
  __fastcall TFormAbout(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAbout *FormAbout;
//---------------------------------------------------------------------------
#endif
