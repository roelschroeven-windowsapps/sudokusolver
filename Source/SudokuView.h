//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#ifndef SudokuViewH
#define SudokuViewH
//---------------------------------------------------------------------------

#include "SudokuModel.h"

const int SUDOKU_ROWS = 9;
const int SUDOKU_COLS = 9;

class TSudokuView: public TCustomControl
{
public:
  __fastcall TSudokuView(TComponent *Owner);
  __fastcall ~TSudokuView();

  DYNAMIC void __fastcall KeyDown(Word &Key, Classes::TShiftState Shift);
  DYNAMIC void __fastcall KeyPress(System::WideChar &Key);
  DYNAMIC void __fastcall MouseUp(TMouseButton Button, Classes::TShiftState Shift, int X, int Y);

  TSudokuModel Cells;
  bool Playing;
  void BeginUpdate();
  void EndUpdate();

  struct TPos
  {
    int row;
    int col;
    TPos(int r, int c): row(r), col(c) { }
    bool operator==(const TPos &other) { return row == other.row && col == other.col; }
    bool operator!=(const TPos &other) { return ! (*this == other); }
  };
  void SetSelected(TPos Selection);
  TPos GetSelected() { return m_Selection; }
  __property TPos Selected = { read = GetSelected, write = SetSelected };

private:
  virtual void __fastcall WndProc(Messages::TMessage &Message);

  void DrawGrid();
  void DrawCell(int row, int col);
  void DrawCells();
  void __fastcall Paint();
  void OnChangeCell(int row, int col);
  void OnChangeAll();

  void MoveSelection(int HorzDelta, int VertDelta);
  void SetSelection(int row, int col);

  TPos m_Selection;
  TRect m_GridRect;
  int m_UpdateLevel;
};

#endif
