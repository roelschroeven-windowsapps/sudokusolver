//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Util.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

void OutputDebugStringF(const char *format, ...)
{
  va_list arg;
  va_start(arg, format);

  AnsiString s;
  s.vprintf(format, arg);
  OutputDebugString(s.c_str());

  va_end(arg);
}

AnsiString AnsiPrintF(const char *format, ...)
{
  va_list arg;
  va_start(arg, format);
  AnsiString s;
  s.vprintf(format, arg);
  va_end(arg);
  return s;
}



THighPrecTimer::THighPrecTimer(bool Start_):
  m_Begin(0.0),
  m_Duration(0.0)
{
  if (Start_)
    Start();
}

void THighPrecTimer::Start()
{
  QueryPerformanceCounter((LARGE_INTEGER*)&m_Begin);
}

void THighPrecTimer::Stop()
{
  __int64 End;
  QueryPerformanceCounter((LARGE_INTEGER*)&End);
  m_Duration = End - m_Begin;
}

double THighPrecTimer::Seconds()
{
  __int64 Frequency;
  QueryPerformanceFrequency((LARGE_INTEGER*)&Frequency);
  return double(m_Duration)/double(Frequency);
}
