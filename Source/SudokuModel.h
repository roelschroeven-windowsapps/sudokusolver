//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#ifndef SudokuModelH
#define SudokuModelH
//---------------------------------------------------------------------------

const int SUDOKU_WIDTH = 9;
const int SUDOKU_HEIGHT = 9;

typedef char TSudokuValue;
const TSudokuValue SudokuValues[] = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };

TSudokuValue NextValue(TSudokuValue Value) { return Value + 1; };

struct TSudokuCell
{
  bool Known;
  bool Given;
  TSudokuValue Value;
  AnsiString ValueString() const;
  TSudokuCell();
};

class TSudokuModel
{
friend class TSudokuView;
public:
  TSudokuModel();
  TSudokuModel(const TSudokuModel &other);
  TSudokuModel& operator=(const TSudokuModel &other);

  void LoadFromFile(AnsiString Filename);
  void SaveToFile(AnsiString Filename);
  void Clear();
  void ResetToGivens();

  __property bool Known[int row][int col] = { read = GetKnown, write = SetKnown };
  __property bool Given[int row][int col] = { read = GetGiven, write = SetGiven };
  __property TSudokuValue Value[int row][int col] = { read = GetValue, write = SetValue };
  __property AnsiString ValueString[int row][int col] = { read = GetValueString };
  __property const TSudokuCell& Cell[int row][int col] = { read = GetCell };
  void Set(int row, int col, bool Known, bool Given, TSudokuValue Value)
  {
    TSudokuCell &Cell(Cell_(row, col));
    Cell.Known = Known;
    Cell.Given = Given;
    Cell.Value = Value;
    EmitChangeCell(row, col);
  }

private:
  TSudokuCell Cells[SUDOKU_WIDTH * SUDOKU_HEIGHT];
  void Clear_();

  const TSudokuCell& Cell_(int row, int column) const { return Cells[row * SUDOKU_WIDTH + column]; }
  TSudokuCell& Cell_(int row, int column) { return Cells[row * SUDOKU_WIDTH + column]; }

  bool GetKnown(int row, int col) const { return Cell_(row, col).Known; }
  void SetKnown(int row, int col, bool Known) { Cell_(row, col).Known = Known; EmitChangeCell(row, col); }
  bool GetGiven(int row, int col) const { return Cell_(row, col).Given; }
  void SetGiven(int row, int col, bool Given) { Cell_(row, col).Given = Given; EmitChangeCell(row, col); }
  TSudokuValue GetValue(int row, int col) const { return Cell_(row, col).Value; }
  void SetValue(int row, int col, TSudokuValue Value) { Cell_(row, col).Value = Value; EmitChangeCell(row, col); }
  AnsiString GetValueString(int row, int col) const;
  const TSudokuCell& GetCell(int row, int col) const { return Cell_(row, col); }

  typedef void (__closure *TChangeCellEvent)(int row, int col);
  TChangeCellEvent OnChangeCell;
  void EmitChangeCell(int row, int col) { if (OnChangeCell) OnChangeCell(row, col); }
  typedef void (__closure *TChangeAllEvent)();
  TChangeAllEvent OnChangeAll;
  void EmitChangeAll() { if (OnChangeAll) OnChangeAll(); }
};

#endif
