//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SudokuModel.h"
#include "Util.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//
// TSudokuCell
//

AnsiString TSudokuCell::ValueString() const
{
  return AnsiString(&Value, 1);
}

TSudokuCell::TSudokuCell():
  Known(false),
  Given(false),
  Value(SudokuValues[0])
{
}


//
// TSudokuModel
//

TSudokuModel::TSudokuModel():
  OnChangeCell(NULL),
  OnChangeAll(NULL)
{
}

TSudokuModel::TSudokuModel(const TSudokuModel &other):
  OnChangeCell(NULL),
  OnChangeAll(NULL)
{
  for (int i = 0; i < ARRAY_SIZE(Cells); ++i)
    Cells[i] = other.Cells[i];
}

TSudokuModel& TSudokuModel::operator=(const TSudokuModel &other)
{
  if (&other == this)
    return *this;

  for (int i = 0; i < ARRAY_SIZE(Cells); ++i)
    Cells[i] = other.Cells[i];

  EmitChangeAll();
  return *this;
}

void TSudokuModel::LoadFromFile(AnsiString Filename)
{
  TStringList *pLines = new TStringList;
  pLines->LoadFromFile(Filename);
  if (pLines->Count != 1 || pLines->Strings[0].Length() != ARRAY_SIZE(Cells))
    {
    delete pLines;
    throw Exception("Incorrect file format");
    }
  AnsiString Values = pLines->Strings[0];
  delete pLines;

  Clear_();
  for (int i = 0; i < ARRAY_SIZE(Cells); ++i)
  {
    if (Values[i + 1] >= '1' && Values[i + 1] <= '9')
    {
      Cells[i].Value = Values[i + 1];
      Cells[i].Given = true;
      Cells[i].Known = true;
    }
  }

  EmitChangeAll();
}

void TSudokuModel::SaveToFile(AnsiString Filename)
{
  AnsiString Values;
  for (int i = 0; i < ARRAY_SIZE(Cells); ++i)
  {
    if (Cells[i].Known && Cells[i].Given)
      Values += Cells[i].Value;
    else
      Values += '.';
  }

  TStringList *pLines = new TStringList;
  pLines->Add(Values);
  pLines->SaveToFile(Filename);
  delete pLines;
}

void TSudokuModel::Clear_()
{
  TSudokuCell EmptyCell;
  for (int i = 0; i < ARRAY_SIZE(Cells); ++i)
    Cells[i] = EmptyCell;
}

void TSudokuModel::Clear()
{
  Clear_();
  EmitChangeAll();
}

void TSudokuModel::ResetToGivens()
{
  TSudokuCell EmptyCell;
  for (int i = 0; i < ARRAY_SIZE(Cells); ++i)
  {
    TSudokuCell &Cell(Cells[i]);
    if (!(Cell.Known && Cell.Given))
      Cell = EmptyCell;
  }
  EmitChangeAll();
}

AnsiString TSudokuModel::GetValueString(int row, int col) const
{
  return Cell_(row, col).ValueString();
}

