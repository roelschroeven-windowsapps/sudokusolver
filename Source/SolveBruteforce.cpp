//---------------------------------------------------------------------------
// Sudoku Solver, a simplistic and naive sudoku solver
// Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
//
// This file is part of Sudoku Solver.
//
// Sudoku Solver is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Sudoku Solver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <stack>

#include "SolveBruteforce.h"
#include "SudokuModel.h"
#include "Util.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

struct TPos
{
  int row;
  int col;
  TPos(int row, int col): row(row), col(col) { }

  void Next()
  {
    col += 1;
    if (col == SUDOKU_WIDTH)
    {
      col = 0;
      row += 1;
    }
  }

  void Previous()
  {
    if (col > 0)
      col -= 1;
    else
    {
      col = SUDOKU_WIDTH - 1;
      row -= 1;
    }
  }

  bool operator!=(const TPos &other)
  {
    return row != other.row || col != other.col;
  }
};

static bool IsUniqueInRow(const TSudokuModel &Model, int row, TSudokuValue Value);
static bool IsUniqueInCol(const TSudokuModel &Model, int col, TSudokuValue Value);
static bool IsUniqueInRegion(const TSudokuModel &Model, int RegionRow, int RegionCol, TSudokuValue Value);
inline static bool IsValidCandidate(const TSudokuModel &Model, const TPos &Pos);

static bool IsFull(const TSudokuModel &Model);

inline static void ResetCell(TSudokuModel &Model, const TPos &Pos);
inline static void Apply(TSudokuModel &Model, const TPos &Pos, TSudokuValue Value);
static void AdvancePos(const TSudokuModel &Model, TPos &Pos);
static void RetreatPos(const TSudokuModel &Model, TPos &Pos);

inline static TSudokuValue NextCandidate(TSudokuModel &Model, TPos &Pos);

inline bool CallbackRequestedStop(TCallback Callback, const TSudokuModel &Model)
{
  bool Stop = false;
  if (Callback) Callback(Model, Stop);
  return Stop;
}

bool SolveBruteforce(TSudokuModel &M, TCallback Callback)
{
  // Start position: first non-given position
  TPos Pos(0, -1);
  AdvancePos(M, Pos);

  while (true)
  {
    //
    // Take next candidate number and put it in the grid
    //

    Apply(M, Pos, NextCandidate(M, Pos));
    if (IsValidCandidate(M, Pos))
    {
      //
      // Doesn't invalidate any constraints so far; keep it (at least for now)
      //
      
      if (CallbackRequestedStop(Callback, M))
        return false;

      if (IsFull(M))
        return true;    // All numbers in the grid are valid, so we reached a solution
      // Advance to next position (skipping cells with given values)
      AdvancePos(M, Pos);
    }
    else
    {
      //
      // Invalid candidate
      //

      while (M.Value[Pos.row][Pos.col] == SudokuValues[ARRAY_SIZE(SudokuValues) - 1])
      {
        //
        // Undo exhausted invalid candidates
        //

        ResetCell(M, Pos);
        RetreatPos(M, Pos);
        if (Pos.row < 0)
          return false;   // Even the very first number doesn't have a single valid value -> no solution can be found
      }
    }
  }
}


static bool IsUniqueInRow(const TSudokuModel &Model, int row, TSudokuValue Value)
{
  unsigned Count = 0;
  for (int col = 0; col < SUDOKU_WIDTH; ++col)
    if (Model.Known[row][col] && Model.Value[row][col] == Value)
      if (++Count > 1)
        return false;
  return true;
}

static bool IsUniqueInCol(const TSudokuModel &Model, int col, TSudokuValue Value)
{
  unsigned Count = 0;
  for (int row = 0; row < SUDOKU_HEIGHT; ++row)
    if (Model.Known[row][col] && Model.Value[row][col] == Value)
      if (++Count > 1)
        return false;
  return true;
}

static bool IsUniqueInRegion(const TSudokuModel &Model, int RegionRow, int RegionCol, TSudokuValue Value)
{
  unsigned Count = 0;
  for (int row = 3 * RegionRow; row < 3 * (RegionRow + 1); ++row)
    for (int col = 3 * RegionCol; col < 3 * (RegionCol + 1); ++col)
      if (Model.Known[row][col] && Model.Value[row][col] == Value)
        if (++Count > 1)
          return false;
  return true;
}

static bool IsValidCandidate(const TSudokuModel &Model, const TPos &Pos)
{
  TSudokuValue Value = Model.Value[Pos.row][Pos.col];
  return IsUniqueInRow(Model, Pos.row, Value)
         && IsUniqueInCol(Model, Pos.col, Value)
         && IsUniqueInRegion(Model, Pos.row/3, Pos.col/3, Value);
}

static bool IsFull(const TSudokuModel &Model)
{
  for (int row = 0; row < SUDOKU_HEIGHT; ++row)
    for (int col = 0; col < SUDOKU_WIDTH; ++col)
     if (!Model.Known[row][col])
       return false;
  return true;
}

static void ResetCell(TSudokuModel &Model, const TPos &Pos)
{
  Model.Known[Pos.row][Pos.col] = false;
}

static void Apply(TSudokuModel &Model, const TPos &Pos, TSudokuValue Value)
{
  Model.Set(Pos.row, Pos.col, true, false, Value);
}

static void AdvancePos(const TSudokuModel &Model, TPos &Pos)
{
  Pos.Next();
  while (Model.Given[Pos.row][Pos.col])
    Pos.Next();
}

static void RetreatPos(const TSudokuModel &Model, TPos &Pos)
{
  Pos.Previous();
  while (Model.Given[Pos.row][Pos.col])
    Pos.Previous();
}

static TSudokuValue NextCandidate(TSudokuModel &Model, TPos &Pos)
{
  if (!Model.Known[Pos.row][Pos.col])
    return SudokuValues[0];
  return NextValue(Model.Value[Pos.row][Pos.col]);
}



