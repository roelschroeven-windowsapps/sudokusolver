;---------------------------------------------------------------------------
; Sudoku Solver, a simplistic and naive sudoku solver
; Copyright (C) 2008 Roel Schroeven <roel@roelschroeven.net>
;
; This file is part of Sudoku Solver.
;
; Sudoku Solver is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; Sudoku Solver is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with Sudoku Solver.  If not, see <http://www.gnu.org/licenses/>.
;---------------------------------------------------------------------------

;
; Setup script for installing Sudoku Solver.
; Compile it with Inno Setup (http://www.jrsoftware.org/isinfo.php)
;

#define SOURCE_DIR="..\App"
#define VERSION GetFileVersion(AddBackslash(SOURCE_DIR) + "SudokuSolver.exe")

[Setup]
SourceDir={#SOURCE_DIR}
SolidCompression=true
Compression=lzma
VersionInfoVersion={#VERSION}
AllowNoIcons=yes
AppName=Sudoku Solver
AppVerName=Sudoku Solver {#VERSION}
DefaultDirName={commonpf}\SudokuSolver
DefaultGroupName=Sudoku Solver
SetupIconFile=..\Sudoku-icon.ico
AppID={{942BBBCD-27D8-43F0-B987-68F7F634202B}
OutputDir=..\Installer
OutputBaseFilename=SudokuSolverSetup
VersionInfoCompany=Roel Schroeven
VersionInfoDescription=Simple solver for 9x9 sudokus
VersionInfoTextVersion={#VERSION}
VersionInfoCopyright=(c) Roel Schroeven
AppPublisher=Roel Schroeven
AppContact=Roel Schroeven <sudokusolver@roelschroeven.net>
AppVersion={#VERSION}
[Files]
Source: SudokuSolver.exe; DestDir: {app}; Flags: touch
Source: *.sud; DestDir: {app}; Flags: touch
Source: ..\COPYING.txt; DestDir: {app}; Flags: touch
Source: ..\SudokuSolverSource.zip; DestDir: {app}; Flags: touch
[Icons]
Name: {group}\Sudoku Solver; Filename: {app}\SudokuSolver.exe
Name: {group}\License; Filename: {app}\COPYING.TXT
